import React from "react";
import {Link} from "react-router-dom";
import './auth-form.scss';
import SignUpForm from "../../components/sign-up-form/sign-up-form";
import AuthSocialMediaButton from "../../components/auth-sm-button/auth-sm-button";

export default function AuthForm() {
    const smButtons = ["facebook", "google"];

    return (
        <div className="auth-form-container">
            <div className="auth-form-header-container">
                <Link to="#" className="auth-form-link auth-form-link__active">Sign Up</Link>
                <Link to="#" className="auth-form-link">Log In</Link>
            </div>
            {smButtons.map((item, index) => <AuthSocialMediaButton title={item} key={index}/>)}
            <div className="divider-block">
                <hr className="auth-form-divider"/>
                <span className="divider-title">or</span>
                <hr className="auth-form-divider"/>
            </div>
            <SignUpForm/>
        </div>
    );
}