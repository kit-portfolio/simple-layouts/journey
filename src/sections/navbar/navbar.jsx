import React from "react";
import {Link} from 'react-router-dom';
import './navbar.scss';
import {ReactComponent as ScheduleIcon} from "../../components/pictograms/navbar/schedule.svg";
import {ReactComponent as ForumIcon} from "../../components/pictograms/navbar/forum.svg";
import {ReactComponent as CompassIcon} from "../../components/pictograms/navbar/compass.svg";

export default function Navbar() {

    return (
        <section className="navbar-section">
            <nav className="navbar-container">
                <Link to="#" className="logo-text">journey</Link>
                <div className="navbar-menu">
                    <Link to="schedule-journey">
                        <ScheduleIcon className="navbar-menu-item"/>
                    </Link>
                    <Link to="forum">
                        <ForumIcon className="navbar-menu-item"/>
                    </Link>
                    <Link to="choose-route">
                        <CompassIcon className="navbar-menu-item"/>
                    </Link>
                </div>
                <p className="navbar-text">Welcome,
                    <Link
                        to="log-in"
                        className="navbar-auth-link margin-right-25">
                        {" Log In"}
                    </Link> or <Link
                        to="sign-up"
                        className="navbar-auth-link">
                        Sign Up</Link>
                </p>
            </nav>
        </section>
    );
}