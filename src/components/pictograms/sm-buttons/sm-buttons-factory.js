import React from "react";
import {ReactComponent as FacebookIcon} from './facebook.svg';
import {ReactComponent as GooglePlusIcon} from "./google-plus.svg";

export default function FSMButtons(target) {
    switch (target) {
        case "facebook": return <FacebookIcon className="sm-button-pictogram"/>;
        case "google": return <GooglePlusIcon className="sm-button-pictogram"/>;
    }
}