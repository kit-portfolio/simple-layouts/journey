import React from 'react';
import "./theme/common.scss";
import "./theme/normalize.scss";
import Navbar from "./sections/navbar/navbar";

function App() {
  return (
    <div className="app">
      <Navbar/>
    </div>
  );
}

export default App;
